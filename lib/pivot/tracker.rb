module Pivot
  class Tracker
    class << self
      def count(items)
        items.count
      end

      def item_for(items, assignee)
        filter_assignee(items, assignee).sort_by { |h| h[:name] }.first
      end

      def pivoted?(items, assignee)
        filter_assignee(items, assignee).count > 1
      end

      def total_points(items, assignee: '')
        if assignee.empty?
          items.sort_by { |h| h[:name] }.uniq { |h| h[:assignee] }.map { |h| h[:points] }.sum
        else
          filter_assignee(items, assignee).map { |i| i[:points] }.sum
        end
      end

      def unique_assignees(items)
        items.map { |h| h[:assignee] }.uniq
      end

      private

      def filter_assignee(items, assignee)
        items.select { |h| h[:assignee] == assignee }
      end

    end
  end
end
