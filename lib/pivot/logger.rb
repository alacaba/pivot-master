require 'logger'

module Pivot
  class Logger < ::Logger
    def initialize(params)
      super(params)
    end
  end
end
