module Pivot
  class Item

    attr_accessor :name, :assignee, :points

    def initialize(options = {})
      @name     = options.fetch(:name, '')
      @assignee = options.fetch(:assignee, '')
      @points   = options.fetch(:points, 0)
    end

    def +(other)
      other.is_a?(Item) ? (@points + other.points) : @points
    end

    def project_code
      @name.split('-').first
    end

    def valid?
      valid_codes = %w[AZR EREC]
      valid_codes.include?(project_code)
    end

  end
end
