module Pivot
  class Person

    attr_accessor :email, :first_name, :last_name, :items

    def initialize(options = {})
      @email      = options.fetch(:email)
      @first_name = options.fetch(:first_name)
      @last_name  = options.fetch(:last_name)
      @items      = []
    end

    def add_item(item)
      yield if block_given?
      item.assignee = email
      items.push(item)
    end
  end
end
